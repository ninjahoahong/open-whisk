#!/bin/bash

pre_build() {
    echo "=== Download and install dependencies"
    sudo apt-get install git npm -y
    git clone https://github.com/apache/openwhisk.git openwhisk
    cd openwhisk
    (cd tools/ubuntu-setup && ./all.sh)
}

build() {
    echo "Build the system"
    ./gradlew distDocker
}

deploy_local() {
    sudo apt-get install python-pip
    sudo pip install ansible==2.5.2
    sudo pip install jinja2==2.9.6
    cp /vagrant/db_local.ini ansible
    cd ansible
    ansible-playbook -i environments/local couchdb.yml
    ansible-playbook -i environments/local initdb.yml
    ansible-playbook -i environments/local wipe.yml
    ansible-playbook -i environments/local openwhisk.yml

    # installs a catalog of public packages and actions
    ansible-playbook -i environments/local postdeploy.yml

    # to use the API gateway
    ansible-playbook -i environments/local apigateway.yml
    ansible-playbook -i environments/local routemgmt.yml
    cd
}

if [ $BASH_SOURCE = $0 ]
then
    pre_build
    build
    deploy
    echo "user run this file as a command"
else
    echo "this file has been source"
fi
